<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Checklist extends Model
{
	protected $table    = 'checklists';
	protected $fillable = [
		'type',
		'created_at',
		'updated_at',
	];

    public function attributes()
    {
        return $this->hasOne(Attribute::class, 'checklist_id');
    }

    public function getCreatedAtAttribute()
    {
        return date('c', strtotime($this->attributes['created_at']));
    }

    public function getCreatedUpdateAttribute()
    {
        return date('c', strtotime($this->attributes['updated_at']));
    }

    public static function boot() {
        parent::boot();

        static::deleting(function($attr) {
             $attr->attributes()->delete();
        });
    }
}