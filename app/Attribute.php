<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
	protected $table   = 'attributes';
	protected $fillable = [
		'checklist_id',
		'object_domain',
		'object_id',
		'description',
		'due',
		'urgency',
		'is_completed',
		'completed_at',
		'last_update_by',
		'updated_at',
		'created_at',
	];

    public function items()
    {
        return $this->hasMany(Item::class);
    }

    public function getCreatedAtAttribute()
    {
        return date('c', strtotime($this->attributes['created_at']));
    }

    public function getCreatedUpdateAttribute()
    {
        return date('c', strtotime($this->attributes['updated_at']));
    }

    public function getIsCompletedAttribute()
    {
        return ($this->attributes['is_completed']) ? "true" : "false";
    }

    public function getDueAttribute()
    {
        return date('c', strtotime($this->attributes['due']));
    }
}
