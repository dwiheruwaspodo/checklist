<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
	protected $table   = 'items';

	protected $fillable = [
		'user_id',
		'attribute_id',
		'checklist_id',
		'name',
		'description',
		'is_completed',
		'due',
		'urgency',
		'updated_by',
	];

    public function attributes()
    {
        return $this->belongsTo(Attribute::class, 'attribute_id');
    }

    public function getCreatedAtAttribute()
    {
        return date('c', strtotime($this->attributes['created_at']));
    }

    public function getCreatedUpdateAttribute()
    {
        return date('c', strtotime($this->attributes['updated_at']));
    }

    public function getIsCompletedAttribute()
    {
        return ($this->attributes['is_completed']) ? "true" : "false";
    }
}