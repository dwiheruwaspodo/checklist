<?php

namespace App\Http\Requests;

class RuleRequest
{
    use RequestAuth;
    use RequestChecklist;
    use RequestItem;
}
