<?php 

namespace App\Http\Requests;

use Validator;

trait RequestAuth {
	
    static function requestAuth($request) 
    {
    	$validate = Validator::make($request->json()->all(), [
			'password' => 'required',
			'email'    => 'required|email'
	    ]);

	    if ($validate->fails()) {
	    	return $validate->errors();
	    }

	    return false;
    }
}
