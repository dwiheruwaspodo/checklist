<?php 

namespace App\Http\Requests;

use Validator;

trait RequestChecklist {

    static function RequestChecklistCreate($request) 
    {
    	$validate = Validator::make($request->json()->all(), [
			'data.attributes'               => 'required',
			'data.attributes.object_domain' => 'required',
			'data.attributes.object_id'     => 'required',
			'data.attributes.description'   => 'required',
			'data.attributes.items'         => 'required|array',
	    ]);

	    if ($validate->fails()) {
	    	return $validate->errors();
	    }

	    return false;
    }

    static function RequestChecklistUpdate($request) 
    {
    	$validate = Validator::make($request->json()->all(), [
			'data.type'                     => 'required|string',
			'data.id'                       => 'required|integer',
			'data.links.self'               => 'required',
			'data.attributes.object_domain' => 'required',
			'data.attributes.object_id'     => 'required',
			'data.attributes.description'   => 'required',
			'data.attributes.is_completed'  => 'required|boolean',
			'data.attributes.completed_at'  => '',
			'data.attributes.created_at'    => 'required'
	    ]);

	    if ($validate->fails()) {
	    	return $validate->errors();
	    }

	    return false;
    }
}
