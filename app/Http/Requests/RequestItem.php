<?php 

namespace App\Http\Requests;

use Validator;

trait RequestItem {

    static function RequestItemComplete($request) 
    {
    	$validate = Validator::make($request->json()->all(), [
			'data'           => 'required',
			'data.*.item_id' => 'required',
	    ]);

	    if ($validate->fails()) {
	    	return $validate->errors();
	    }

	    return false;
    }

    static function RequestItemCreate($request) 
    {
    	$validate = Validator::make($request->json()->all(), [
			'data.attribute.description' => 'required',
			'data.attribute.due'         => 'required',
			'data.attribute.urgency'     => 'required',
	    ]);

	    if ($validate->fails()) {
	    	return $validate->errors();
	    }

	    return false;
    }
}
