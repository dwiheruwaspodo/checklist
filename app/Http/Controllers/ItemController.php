<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\User;
use App\Attribute;
use App\Checklist;
use App\Item;
use DB;
use App\Http\Requests\RuleRequest;
use Illuminate\Http\Response;

class CompleteItem {
    public $items;
    public $flag;
    public $userId;
}

class ItemController extends Controller
{
    const COMPLETE   = 1;
    const INCOMPLETE = 0;

    public $checkListController;

    function __construct()
    {
        $this->checkListController = 'App\Http\Controllers\CheckListController';
    }

    function complete(Request $request)
    {
        DB::beginTransaction();
        try {
            $validate = RuleRequest::RequestItemComplete($request);

            if ($validate) {
                return parent::failResponse($validate, Response::HTTP_UNPROCESSABLE_ENTITY);
            }

            $this->updateItems(self::COMPLETE, $request);

            DB::commit();
            return parent::successResponse($this->queryItems($request->json('data')));

        } catch (\Exception $e) {
            DB::rollback();
            return parent::failResponse("Server Error", Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    function inComplete(Request $request)
    {
        DB::beginTransaction();
        try {
            $validate = RuleRequest::RequestItemComplete($request);

            if ($validate) {
                return parent::failResponse($validate, Response::HTTP_UNPROCESSABLE_ENTITY);
            }

            $this->updateItems(self::INCOMPLETE, $request);

            DB::commit();
            return parent::successResponse($this->queryItems($request->json('data')));

        } catch (\Exception $e) {
            DB::rollback();
            return parent::failResponse("Server Error", Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    private function updateItems(int $type, $request)
    {
        $itemParam         = new CompleteItem();
        $itemParam->items  = $request->json('data');
        $itemParam->flag   = $type;
        $itemParam->userId = $request->user()->id;

        return $this->updateAllItem($itemParam);
    }

    private function updateAllItem(CompleteItem $param)
    {
        return array_walk($param->items, array($this, 'findItem'), [
            'is_completed' => $param->flag, 
            'updated_by'   => $param->userId
        ]);
    }

    private function findItem($item, $key, array $data)
    {
        $itemExist = Item::find($item['item_id']);
        return ($itemExist) ? $this->updateDataItem($itemExist, $data) : true;
    }

    private function updateDataItem($itemExist, array $data)
    {
        return $itemExist->update($data);
    }

    function queryItems(array $items)
    {
        return Item::select('id', 'is_completed', 'checklist_id')->whereIn('id', array_column($items, 'item_id'))->get();
    }

    function create(Request $request, int $checklistId)
    {
        $validate = RuleRequest::RequestItemCreate($request);

        if ($validate) {
            return parent::failResponse($validate, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $attributeId = app($this->checkListController)->getAttributeId($checklistId);

        if (!$attributeId) {
            return parent::failResponse("Failed to save", Response::HTTP_BAD_REQUEST);
        }
        
        $item               = new Item();            
        $item->due          = date('Y-m-d H:i:s', strtotime($request->json('data')['attribute']['due']));
        $item->name         = $request->json('data')['attribute']['description'];
        $item->attribute_id = $attributeId;
        $item->user_id      = $request->user()->id;
        $item->checklist_id = $checklistId;

        if ($item->save()) {
            return parent::successResponse(app($this->checkListController)->resultSave($checklistId));
        }

        return parent::failResponse("Failed to save", Response::HTTP_BAD_REQUEST);
    }

    function detail(Request $request, int $checklistId, int $itemId)
    {
        $items = $this->querySelectedWithItem($checklistId, $itemId);

        if (!$items) {
            return parent::failResponse("Not Found", Response::HTTP_NOT_FOUND);
        }

        $items->toArray()['links'] = ['self' => url("/checklists/{$checklistId}")];

        return parent::successResponse($items);
    }


    function selected(Request $request, int $checklistId)
    {
        $items = $this->querySelected($checklistId);

        if (!$items) {
            return parent::failResponse("Not Found", Response::HTTP_NOT_FOUND);
        }
        
        $items->toArray()['links'] = ['self' => url("/checklists/{$checklistId}")]; 
        
        return parent::successResponse($items);
    }

    function querySelected(int $checklistId)
    {
        return Checklist::select('type', 'id')->with(['attributes.items'])->find($checklistId);
    }

    function querySelectedWithItem(int $checklistId, int $itemId)
    {
        return Checklist::select('type', 'id')->with(['attributes.items' => function($query) use ($itemId) {
                    $query->find($itemId);
        }])->find($checklistId);
    }

    function update(Request $request, $checklistId, $itemId)
    {
        $validate = RuleRequest::RequestItemCreate($request);

        if ($validate) {
            return parent::failResponse($validate, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $attributeId = app($this->checkListController)->getAttributeId($checklistId);

        if (!$attributeId) {
            return parent::failResponse("Not Found", Response::HTTP_NOT_FOUND);
        }

        $item               = Item::find($itemId);    
        $item->due          = date('Y-m-d H:i:s', strtotime($request->json('data')['attribute']['due']));
        $item->name         = $request->json('data')['attribute']['description'];
        $item->attribute_id = $attributeId;
        $item->user_id      = $request->user()->id;
        $item->checklist_id = $checklistId;

        if ($item->save()) {
            return parent::successResponse(app($this->checkListController)->resultSave($checklistId));
        }

        return parent::failResponse("Failed to save", Response::HTTP_BAD_REQUEST);
    }

    function delete(Request $request, $checklistId, $itemId)
    {
        $existData = Checklist::find($checklistId);

        if ($existData) {
            if (Item::where('attribute_id', $existData->attributes()->first()->id)->where('id', $itemId)->delete()) {
                return parent::successResponse(app($this->checkListController)->resultSave($checklist_id));
            }
        }

        return parent::failResponse("Failed to delete", Response::HTTP_CONFLICT);
    }

}
