<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\User;
use App\Attribute;
use App\Checklist;
use App\Item;
use DB;
use App\Http\Requests\RuleRequest;
use Illuminate\Http\Response;

class ItemSaveParams {
    public $attribute;
    public $checklist;
    public $user;
    public $items;
}

class CheckListController extends Controller
{
    const TYPE_CHECKLIST = 'checklists';
	
    function getAttributeId(int $checklist_id) : int {
        $checklist = Checklist::find($checklist_id);
        return ($checklist) ? $checklist->attributes()->first()->id : 0;
    }

    function create(Request $request)
    {
    	DB::beginTransaction();
        try {
	    	$validate = RuleRequest::RequestChecklistCreate($request);

	    	if ($validate) {
	    		return parent::failResponse($validate, Response::HTTP_UNPROCESSABLE_ENTITY);
	    	}

	    	$saveChecklist = Checklist::create(['type' => self::TYPE_CHECKLIST]);

	    	if (!$saveChecklist) {
                DB::rollback();
                return parent::failResponse("Failed to save", Response::HTTP_BAD_REQUEST);
            }

    		$saveAttr = $this->saveAttributes($saveChecklist, $request->json('data')['attributes']);

    		if (!$saveAttr) {
                DB::rollback();
                return parent::failResponse("Failed to save", Response::HTTP_BAD_REQUEST);  
    		}

            $item            = new ItemSaveParams();
            $item->attribute = $saveAttr;
            $item->checklist = $saveChecklist;
            $item->user      = $request->user()->id;
            $item->items     = $request->json('data')['attributes'];

            $saveItems = $this->saveItems($item);

            if (!$saveItems) {
                DB::rollback();
                return parent::failResponse("Failed to save", Response::HTTP_BAD_REQUEST);
            }

    		DB::commit();
    		return parent::successResponse($this->resultSave($saveChecklist->id));
	    	
    	} catch (\Exception $e) {
            DB::rollback();
            return parent::failResponse("Server Error", Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    function saveAttributes($checklist, $dataAttr)
    {
        return Attribute::create([
            'checklist_id'  => $checklist->id,
            'due'           => date('Y-m-d H:i:s', strtotime($dataAttr['due'])),
            'object_domain' => $dataAttr['object_domain'],
            'object_id'     => $dataAttr['object_id'],
            'urgency'       => $dataAttr['urgency'],
            'description'   => $dataAttr['description'],
        ]);
    }

    function saveItems(ItemSaveParams $param)
    {
    	if (isset($param->items['items']) && !empty($param->items['items'])) {
            foreach ($param->items['items'] as $key => $item) {
                $dataItems[] = [
                    'name'         => $item,
                    'user_id'      => $param->user,
                    'created_at'   => date('Y-m-d H:i:s'),
                    'updated_at'   => date('Y-m-d H:i:s'),
                    'checklist_id' => $param->checklist->id,
                    'attribute_id' => $param->attribute->id
                ];
    		}

            return Item::insert($dataItems);
    	}

    	return true;
    }

    function resultSave($checklistId)
    {
		$data          = Checklist::select('type', 'id')->where('id', $checklistId)->with('attributes')->first()->toArray();
		$data['links'] = ['self' => url("/checklists/{$checklistId}")];

    	return $data;
    }

    function update(Request $request, int $checklistId)
    {
    	DB::beginTransaction();

    	try {
	    	$validate = RuleRequest::RequestChecklistUpdate($request);

	    	if ($validate) {
	    		return parent::failResponse($validate, Response::HTTP_UNPROCESSABLE_ENTITY);
	    	}

	    	$checklist = Checklist::find($checklistId);

	    	if (!$checklist) {
                DB::rollback();
                return parent::failResponse("Failed to save", Response::HTTP_BAD_REQUEST);
	    	}

            if (!$checklist->update(['type' => $request->json('data')['type']])) {
                DB::rollback();
                return parent::failResponse("Failed to save", Response::HTTP_BAD_REQUEST);
            }

            if (!$this->updateAttribute($checklist, $request->json('data')['attributes'])) {
                DB::rollback();
                return parent::failResponse("Failed to save", Response::HTTP_BAD_REQUEST);
            }
                
            DB::commit();
            return parent::successResponse($this->resultSave($checklistId));
	    	
    	} catch (\Exception $e) {
            DB::rollback();
            return parent::failResponse("Server Error", Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    private function updateAttribute($checklist, array $dataAttr)
    {
        $dataAttr['created_at']   = date('Y-m-d H:i:s', strtotime($dataAttr['created_at']));
        $dataAttr['completed_at'] = date('Y-m-d H:i:s', strtotime($dataAttr['completed_at']));
        $dataAttr['is_completed'] = ($dataAttr['is_completed']) ? 1 : 0;

        return $checklist->attributes()->update($dataAttr);
    }

    function detail(Request $request, int $checklistId)
    {
    	if (Checklist::find($checklistId)) {
    		return parent::successResponse($this->resultSave($checklistId));
    	}

    	return parent::failResponse("Not Found", Response::HTTP_NOT_FOUND);
    }

    function delete(Request $request, int $checklistId)
    {
    	$checklist = Checklist::find($checklistId);

    	if (!$checklist) {
            return parent::failResponse("Not Found", Response::HTTP_NOT_FOUND); 
        }

		Item::where('attribute_id', $this->getAttributeId($checklistId))->delete();
		
        return ($checklist->delete()) ? parent::successResponse(['msg' => 'success']) : parent::failResponse("Failed to delete", Response::HTTP_CONFLICT); 
    }

    function index(Request $request)
    {
        $offset = $this->handlePagination($request)['offset'];
        $take   = $this->handlePagination($request)['take'];
    	
    	$checklist = Checklist::select('type', 'id')->with(['attributes' => function($query) use ($request) {
            parent::requestfilter($query, $request->filter);
            parent::requestSort($query, $request->sort);
        }, 'attributes.items'])->offset($offset)->take($take)->get()->toArray();

        if ($checklist) {
            $checklist = $this->filteringChecklist($checklist);
        }

        return parent::successResponse([
			'meta'  => $this->getMeta(count($checklist), count($checklist)),
			'links' => $this->getLinks($take, $offset),
			'data'  => $checklist,
        ]);
    }

    private function filteringChecklist($checklist)
    {
        $tempData = array_values(array_filter($checklist, function($value) {
            return (!empty($value['attributes']));
        }));

        return array_map(array($this, 'addLinkKey'), $tempData);
    }

    private function handlePagination($request) : array
    {
        return [
            'take'   => ($request->input('page.limit')) ? $request->input('page.limit') : config('paginate.take'),
            'offset' => ($request->input('page.offset')) ? $request->input('page.limit') : config('paginate.offset')
        ];
    }

    private function getLinks($take, $offset) : array
    {
        return [
            'first' => url("/checklists?page[limit]={$take}&page[offset]={$offset}"),
            'last'  => url("/checklists?page[limit]={$take}&page[offset]={$offset}"),
            'next'  => url("/checklists?page[limit]={$take}&page[offset]={$offset}"),
            'prev'  => null,
        ];
    }

    private function getMeta($count, $total) : array
    {
        return [
            'count' => $count,
            'total' => $total
        ];
    }

    private function addLinkKey($data)
    {
        return $data += ['link' => ['self' => url("/checklists/{$data['id']}")]];
    }

}
