<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\User;

use App\Http\Requests\RuleRequest;

class AuthController extends Controller
{
    const STR_RANDOM = 77;

    public function authenticate(Request $request)
    {
        $validate = RuleRequest::requestAuth($request);

        if ($validate) {
            return parent::failResponse($validate, Response::HTTP_BAD_REQUEST);
        }

        $user = User::where('email', $request->json('email'))->first();

        if ($user) {
            if ($checkCredential = $this->checkCredential($request, $user)) {
                return parent::successResponse($checkCredential);
            }
        }

        return parent::failResponse("email & password didn't match", Response::HTTP_UNAUTHORIZED);
    }

    public function checkCredential($request, $user)
    {
        if(app('hash')->check($request->input('password'), $user->password)){
            $apikey = base64_encode(str_random(self::STR_RANDOM));
            $this->updateTokenUser($request->json('email'), ['api_token' => $apikey]);
            return ['token' => $apikey];
        }

        return false;
    }

    public function updateTokenUser(String $email, Array $data)
    {
        return User::where('email', $email)->update($data);   
    }

    public function whoIam(Request $request)
    {
        return parent::successResponse($request->user());
    }
}
