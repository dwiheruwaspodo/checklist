<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Response;

class Controller extends BaseController
{
    const IDENTIFIER_LIKE = '*';

    static function failResponse(string $msg, int $code)
    {
        return response()->json(['status' => $code, 'error' => $msg]);
    }

    static function failResponseNoMsg(int $code)
    {
        return response()->json(['status' => $code]);
    }

    static function successResponse($data)
    {
    	return response()->json(['status' => Response::HTTP_OK, 'data' => $data]);
    }

    static function requestfilter($query, array $filter)
    {
    	return array_walk($filter, array(new Controller, 'filterWord'), $query);
    }

    private function filterWord($word, $field, $query)
    {
        $data = ['query' => $query, 'field' => $field];

        return (in_array($field, self::allowField())) ? array_walk($word, array($this, 'processOperator'), $data) : $query;
    }

    private function processOperator($word, $operator, $data)
    {
        return $data['query']->where($data['field'], self::symbol($operator), $this->handleLikeOperator($operator, $word));
    }

    private function handleLikeOperator($operator, $word)
    {
        return ($operator == 'like') ? $this->handlePlacementLike($word) : $word;
    }

    private function handlePlacementLike($word)
    {
        return (stristr($word, self::IDENTIFIER_LIKE)) ? str_replace(self::IDENTIFIER_LIKE, '%', $word) : true;
    }

    static function requestSort($query, string $sort)
    {
		if ($sort) {
			if (in_array($sort, self::allowField())) {
	    		$query->orderBy($sort, self::orderFilter($sort));
			}	
		}

    	return $query;
    }

    static function orderFilter(string $sort)
    {
        return (stristr($sort, "-")) ? 'DESC' : 'ASC';
    }

    static function allowField()
    {
    	return ['object_domain', 'object_id', 'description', 'is_completed', 'completed_at'];
    }

    static function symbol(string $symbol)
    {
    	switch ($symbol) {
    		case 'like':
    			return 'LIKE';
    			break;
    		case '!like':
    			return 'NOT LIKE';
    			break;
    		case 'is':
    			return '=';
    			break;
    		case '!is':
    			return '!=';
    			break;
    		case 'in':
    			return 'IN';
    			break;
    		case '!in':
    			return 'NOT IN';
    			break;
    		
    		default:
    			return '=';
    			break;
    	}
    }
}
