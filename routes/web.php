<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return response()->json(['version' => $router->app->version()]);
});

$router->post('login', 'AuthController@authenticate');

$router->group(['middleware' => 'auth'], function ($router) {
	$router->get('user', 'AuthController@whoIam');

	$router->get('checklists', 'CheckListController@index');
	$router->post('checklists', 'CheckListController@create');
	
	$router->patch('checklists/{id}', 'CheckListController@update');
	$router->get('checklists/{id}', 'CheckListController@detail');
	$router->delete('checklists/{id}', 'CheckListController@delete');

	$router->post('checklists/complete', 'ItemController@complete');
	$router->post('checklists/incomplete', 'ItemController@inComplete');
	
	$router->get('checklists/{checklist_id}/items', 'ItemController@selected');
	$router->post('checklists/{checklist_id}/items', 'ItemController@create');

	$router->get('checklists/{checklist_id}/items/{item_id}', 'ItemController@detail');
	$router->patch('checklists/{checklist_id}/items/{item_id}', 'ItemController@update');
	$router->delete('checklists/{checklist_id}/items/{item_id}', 'ItemController@delete');
});

