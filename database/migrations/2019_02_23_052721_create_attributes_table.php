<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attributes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('checklist_id')
                ->foreign('checklist_id')
                ->references('id')->on('checklists')
                ->onDelete('cascade');
            $table->string('object_domain', 60);
            $table->string('object_id', 10);
            $table->dateTime('due')->nullable();
            $table->tinyInteger('urgency')->default(0);
            $table->string('description', 255);
            $table->boolean('is_completed')->default(false);
            $table->dateTime('completed_at')->nullable();
            $table->string('last_update_by', 60)->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attributes');
    }
}
