<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');

            $table->unsignedInteger('attribute_id')
                ->foreign('attribute_id')
                ->references('id')->on('attributes')
                ->onDelete('cascade');

            $table->string('checklist_id')->nullable();
            $table->string('name', 60);
            $table->string('description', 100)->nullable();
            $table->boolean('is_completed')->default(false);
            $table->string('due', 20)->nullable();
            $table->tinyInteger('urgency')->default(0);
            $table->string('updated_by', 60)->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
