<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        User::create([
			'name'     => 'kong',
			'email'    => 'kong@kw.com',
			'password' => app('hash')->make('123')
        ]);
    }
}
